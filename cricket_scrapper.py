
# -*- coding: utf-8 -*-
import requests
import time
from datetime import datetime
from os import listdir
from os.path import isfile, join
import json
import time                   
#import schedule
import requests

MessageBuffer = set()
def telegram_bot_sendtext(bot_message):     
    bot_token = '1133478849:AAFLXmi6UwJpxsXh2_62ESjoTOvW6TGN4Bw'
    bot_chatID = '-1001330405233'
    #bot_chatID = '-498987998'
    send_text = 'https://api.telegram.org/bot' + bot_token + '/sendMessage?chat_id=' + bot_chatID + '&parse_mode=Markdown&text=' + bot_message
    response = requests.get(send_text)
    print(response)
    return response.json()

def GetFiles():
    onlyfiles = [f for f in listdir('data') if isfile(join('data', f))]
    return onlyfiles

def SaveJson(response):
    file = open("data/" + str(datetime.now()) + ".text", 'w')
    file.write(str(response))
    file.close()
    
def PrevInning(cricket_data):
    Message = ''
    if(cricket_data.get("miniscore")):
        TeamName = cricket_data["miniscore"]["matchScoreDetails"]["inningsScoreList"][-1]["batTeamName"] 
        Score = str(cricket_data["miniscore"]["matchScoreDetails"]["inningsScoreList"][-1]["score"])
        Wicket = str(cricket_data["miniscore"]["matchScoreDetails"]["inningsScoreList"][-1].get("wickets"))
        over =  str(cricket_data["miniscore"]["matchScoreDetails"]["inningsScoreList"][-1]["overs"]) + "Ov"
        Message = TeamName + ' ' + Score + '/' + Wicket +  "    "  + over
        return Message
    return Message


def score_string(cricket_data):
    Message = ''
    if(cricket_data.get("miniscore")):
        for data in cricket_data["miniscore"]["matchScoreDetails"]["inningsScoreList"][::-1]: # list last value is last inning data
            TeamName = data["batTeamName"] 
            Score = str(data["score"])
            Wicket = str(data.get("wickets"))
            over =  str(data["overs"]) + "Ov"
            Message = Message + TeamName + ' ' + Score + '/' + Wicket +  "    "  + over + "\n"
        return Message
    return Message
    

def GenerateMsgCurrInnPrevInn(cricket_data):
     message =BattingPlayer1=BattingPlayer2= ''
     previnn = score_string(cricket_data)
     team1 = cricket_data["matchHeader"]['team1']['shortName']
     team2 = cricket_data["matchHeader"]['team2']['shortName']
     currentTeam = cricket_data["commentaryList"][-1].get("batTeamName")
     if(cricket_data.get("miniscore")):
        miniscore = cricket_data.get("miniscore")
        if(miniscore.get("batsmanStriker")):
            if (miniscore["batsmanStriker"].get('batName')):
                BattingPlayer1 = "Batting Now: " + miniscore["batsmanStriker"].get('batName') + '(' + str(miniscore["batsmanStriker"].get("batRuns"))+')'
            if(miniscore["batsmanNonStriker"].get('batName')):
                BattingPlayer2 = miniscore["batsmanNonStriker"].get('batName') + '(' + str(miniscore["batsmanNonStriker"].get("batRuns")) + ')'
        message = "🏏".decode('utf-8') + str(team1) + " Vs " + str(team2) + "\n" + previnn 
        lastwicket = "LastWicket : " + miniscore["lastWicket"]  if miniscore.get("lastWicket") else "" 
        message = message +'\n' + BattingPlayer1 + ', ' + BattingPlayer2 + '\n' + str(lastwicket) 
     return message

def main(url):
    cricket_data = requests.get(url).json()
    #DataFiles = GetFiles()
    if(cricket_data["matchHeader"].get("status") == "Innings Break"  or cricket_data["matchHeader"].get("status") == "Complete"):
         return
    Message = GenerateMsgCurrInnPrevInn(cricket_data)     
    if Message and Message not in MessageBuffer:
        print(Message)
        telegram_bot_sendtext(Message)
        MessageBuffer.add(Message)
    else:
        print("No message found")
        return
    SaveJson(cricket_data)

    """for i in DataFiles:
        print(i)
        f = open('data/'+i,) 
        cricket_data = json.dumps(f) 
        f.close()
        GenerateMsgCurrInnPrevInn(cricket_data)"""
    

if __name__ == "__main__":
    #url = "https://www.cricbuzz.com/api/cricket-match/commentary/30235"
    #url = "https://www.cricbuzz.com/api/cricket-match/commentary/30230"
    #url ="https://www.cricbuzz.com/api/cricket-match/commentary/30114"
    url = "https://www.cricbuzz.com/api/cricket-match/commentary/30350"
    while True:
        main(url)
        time.sleep(60)